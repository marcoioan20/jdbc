import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import autores.Autor;
import autores.AutorController;
import escrito_por.Escrito_por;
import escrito_por.Escrito_porController;
import libros.LibrosController;

public class Pruebas {

	public static void main(String[] args) {
		String cadena_conexion = "jdbc:mysql://localhost:3306/libreria?useUnicode=true&serverTimezone=UTC";
		String usuario = "root";
		String contrasena = "1234";

		Connection conn;
		try {
			conn = DriverManager.getConnection(cadena_conexion, usuario, contrasena);
			AutorController ac = new AutorController(conn);
			LibrosController lb = new LibrosController(conn);
			Escrito_porController ep = new Escrito_porController(conn);
			// String cadena = "Select * from autores ORDER by id";

			// List<Autor> rs = ac.selectAllAutores();
			System.out.println("Id |" + "|    DNI   |" + "| Edad |" + "|   Nombre  |" + "|  Apellido  |");
			ResultSet rs = ac.selectAllAutores();
			ResultSet rs1 = lb.selectAllLibros();
			ResultSet rs2 = ep.selectAllEscrito_por();
			while (rs.next()) {
				int id = rs.getInt("nAutores");
				String dni = rs.getString("dni");
				String nombre = rs.getString("nombre");
				String apellido = rs.getString("apellidos");
				int edad = rs.getInt("edad");
				// Autor a1 = new Autor();
				System.out.println(id + " -  " + dni + "     " + edad + "        " + nombre + "       " + apellido);
			}
			System.out.println("\n");
			System.out.println("Id |" + "| Titulo   |" + "| A�o publicacion |" + "| Nr. Paginas |" + "| Editorial |" );
			while (rs1.next()) {
				int id1 = rs1.getInt("nLibros");
				String titulo = rs1.getString("nTitulo");
				int annoPub = rs1.getInt("nAnnoPublicacion");
				int numPag = rs1.getInt("nNumPaginas");
				String editorial = rs1.getString("nEditorial");
				System.out.println(id1 + " - " + titulo + "         " + annoPub + "            " + numPag + "        " + editorial);
			}

			System.out.println("\n");
			System.out.println("Id Autor |" + "| Id Libros" );
			while (rs2.next()) {
				int id2 = rs2.getInt("nAutores");
				int id3 = rs2.getInt("nLibros");
				System.out.println(id2 + "               " + id3);
			}

			rs.close();
			conn.close();

			// String consulta = "select * "+ " from autores;";
			// PreparedStatement ps = conn.prepareStatement(consulta,
			// ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			// System.out.println(ps);

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// insertamos un autor
		/*
		 * Autor autor1 = new Autor(4, "9876543E", "Gloria", "Fuertes", 83); if
		 * (ac.insertAutor(autor1))
		 * System.out.println("El autor se ha insertado correctamente"); else
		 * System.out.println("El autor NO se ha insertado"); }catch(Exception e) {
		 * e.printStackTrace(); }
		 */

	}
}