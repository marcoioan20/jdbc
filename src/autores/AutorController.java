package autores;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class AutorController implements AutorDAO {
	private final String SELECT_ALL = "select * from autores";

	private Connection con;
	
	

	public AutorController(Connection con) {
		super();
		this.con = con;
	}
	@Override
	public ResultSet selectAllAutores(){
		
		
		ResultSet rs = null;
		PreparedStatement stm;
		try {
			stm = con.prepareStatement("SELECT_ALL");
			 rs = stm.executeQuery(SELECT_ALL);
			
			
			//stm.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return rs;
		/*		//List<Autor> autores = new List<Autor>();
		ArrayList<Autor> autores = new ArrayList<Autor>();		
		ResultSet rs;
		PreparedStatement stm; 
		try {
			 stm = con.prepareStatement("select nAutores, dni, nombre, apellidos, edad from autores; ");
			 rs = stm.executeQuery();
			
			 while (rs.next()) {
				 Autor autor = new Autor(rs.getInt("nAutores"), rs.getString("dni"), rs.getString("nombre"),  rs.getString("apellidos"), rs.getInt("edad"));
				 autores.add(autor);
			 }
			stm.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	}
	
	public boolean insertAutor(Autor autor) {
		
		String sql = "insert into autores (nAutores, dni, nombre, apellidos, edad) values (?,?,?,?,?)";
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, autor.getnAutores());
			ps.setString(2,  autor.getDni());
			ps.setString(3, autor.getNombre());
			ps.setString(4,  autor.getApellido());
			ps.setInt(5,  autor.getEdad());
			
			ps.executeUpdate();
			
		}catch (SQLException e) {
			System.out.println("Se ha producido un error al insertar el autor");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	public boolean deleteAutor(int nAutores) {
		String sql = "delete from autores where nAutores = ?";
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, nAutores);
			
			
			ps.executeUpdate();
			
		}catch (SQLException e) {
			System.out.println("Se ha producido un error al borrar el autor");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	public boolean updateAutor(int nAutores, String dni, String nombre, String apellidos, int edad) {
		String sql = "update autores set dni = ?, nombre = ?, apellidos = ?, edad = ? where nAutores = ?";
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			
			
			ps.setString(1, dni);
			ps.setString(2, nombre);
			ps.setString(3,  apellidos);
			ps.setInt(4, edad);
			ps.setInt(5, nAutores);
			
			ps.executeUpdate();
			
		}catch (SQLException e) {
			System.out.println("Se ha producido un error al actualizar el autor");
			e.printStackTrace();
			return false;
		}
		
		return true;		
	}
	
	
}

