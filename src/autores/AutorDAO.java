package autores;
 
import java.sql.ResultSet;


public interface AutorDAO {

	public ResultSet selectAllAutores();

	public boolean insertAutor(Autor autor);

	public boolean deleteAutor(int nAutores);

	public boolean updateAutor(int nAutores, String dni, String nombre, String apellidos, int edad);



}
