package autores;

public class Autor {

	
	private int nAutores;
	private String dni;
	private String nombre;
	private String apellido;
	private int edad;
	
	public Autor(int nAutores, String dni, String nombre, String apellido, int edad) {
		super();
		this.nAutores = nAutores;
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
	}
	public int getnAutores() {
		return nAutores;
	}
	public void setnAutores(int nAutores) {
		this.nAutores = nAutores;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	@Override
	public String toString() {
		return "Autor [nAutores=" + nAutores + ", dni=" + dni + ", nombre=" + nombre + ", apellido=" + apellido
				+ ", edad=" + edad + ", getnAutores()=" + getnAutores() + ", getDni()=" + getDni() + ", getNombre()="
				+ getNombre() + ", getApellido()=" + getApellido() + ", getEdad()=" + getEdad() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	
}
