package libros;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class LibrosController implements LibrosDAO{
	private final String SELECT_ALL = "select * from libros";
	private Connection con;
	
	
	
	public LibrosController(Connection con) {
		super();
		this.con = con;
	}



	
	@Override
	public ResultSet selectAllLibros(){
		
		
		ResultSet rs = null;
		PreparedStatement stm;
		try {
			stm = con.prepareStatement("SELECT_ALL");
			 rs = stm.executeQuery(SELECT_ALL);
			
			
			//stm.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return rs;

	}

	@Override
	public boolean insertLibros(Libros libros) {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public boolean deleteLibros(int nLibros) {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public boolean updateLibros(int nLibros, String nTitulo, int nAnnoPublicacion, int nNumPaginas, String nEditorial) {
		// TODO Auto-generated method stub
		return false;
	}
	

	

	
	
	
}
