package libros;

import java.sql.ResultSet;
import java.util.List;



public interface LibrosDAO {


	public ResultSet selectAllLibros();

	public boolean insertLibros(Libros libros);

	public boolean deleteLibros(int nLibros);

	public boolean updateLibros(int nLibros, String nTitulo, int nAnnoPublicacion, int nNumPaginas, String nEditorial);


	
	
}
