package libros;

public class Libros {

	private int nLibros;
	private String nTitulo;
	private int nAnnoPublicacion;
	private int nNumPaginas;
	private String nEditorial;
	public Libros(int nLibros, String nTitulo, int nAnnoPublicacion, int nNumPaginas, String nEditorial) {
		super();
		this.nLibros = nLibros;
		this.nTitulo = nTitulo;
		this.nAnnoPublicacion = nAnnoPublicacion;
		this.nNumPaginas = nNumPaginas;
		this.nEditorial = nEditorial;
	}
	public int getnLibros() {
		return nLibros;
	}
	public void setnLibros(int nLibros) {
		this.nLibros = nLibros;
	}
	public String getnTitulo() {
		return nTitulo;
	}
	public void setnTitulo(String nTitulo) {
		this.nTitulo = nTitulo;
	}
	public int getnAnnoPublicacion() {
		return nAnnoPublicacion;
	}
	public void setnAnnoPublicacion(int nAnnoPublicacion) {
		this.nAnnoPublicacion = nAnnoPublicacion;
	}
	public int getnNumPaginas() {
		return nNumPaginas;
	}
	public void setnNumPaginas(int nNumPaginas) {
		this.nNumPaginas = nNumPaginas;
	}
	public String getnEditorial() {
		return nEditorial;
	}
	public void setnEditorial(String nEditorial) {
		this.nEditorial = nEditorial;
	}
	@Override
	public String toString() {
		return "Libros [nLibros=" + nLibros + ", nTitulo=" + nTitulo + ", nAnnoPublicacion=" + nAnnoPublicacion
				+ ", nNumPaginas=" + nNumPaginas + ", nEditorial=" + nEditorial + ", getnLibros()=" + getnLibros()
				+ ", getnTitulo()=" + getnTitulo() + ", getnAnnoPublicacion()=" + getnAnnoPublicacion()
				+ ", getnNumPaginas()=" + getnNumPaginas() + ", getnEditorial()=" + getnEditorial() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	

	
	
	
}
