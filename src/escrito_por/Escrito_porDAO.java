package escrito_por;

import java.sql.ResultSet;




public interface Escrito_porDAO {

	public ResultSet selectAllEscrito_por();

	public boolean insertLibros(Escrito_por escrito_por);

	public boolean deleteLibros(int nAutores);

	public boolean updateLibros(int nAutores, int nLibros);

	ResultSet selectAllEscrito();
	
}
