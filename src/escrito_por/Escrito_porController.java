package escrito_por;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Escrito_porController implements Escrito_porDAO {
	private final String SELECT_ALL = "select * from escrito_por";
	private Connection con;

	public Escrito_porController(Connection con) {
		super();
		this.con = con;
	}


	@Override
	public  ResultSet selectAllEscrito_por(){
		
		
		ResultSet rs = null;
		PreparedStatement stm;
		try {
			stm = con.prepareStatement("SELECT_ALL");
			 rs = stm.executeQuery(SELECT_ALL);
			
			
			//stm.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return rs;

	}

	@Override
	public boolean insertLibros(Escrito_por escrito_por) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteLibros(int nAutores) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateLibros(int nAutores, int nLibros) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public ResultSet selectAllEscrito() {
		// TODO Auto-generated method stub
		return null;
	}





}
