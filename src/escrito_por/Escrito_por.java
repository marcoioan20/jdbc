package escrito_por;

public class Escrito_por {
private int nAutores;
private int nLibros;
public Escrito_por(int nAutores, int nLibros) {
	super();
	this.nAutores = nAutores;
	this.nLibros = nLibros;
}
public int getnAutores() {
	return nAutores;
}
public void setnAutores(int nAutores) {
	this.nAutores = nAutores;
}
public int getnLibros() {
	return nLibros;
}
public void setnLibros(int nLibros) {
	this.nLibros = nLibros;
}
@Override
public String toString() {
	return "Escrito_por [nAutores=" + nAutores + ", nLibros=" + nLibros + ", getnAutores()=" + getnAutores()
			+ ", getnLibros()=" + getnLibros() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
			+ ", toString()=" + super.toString() + "]";
}




}
